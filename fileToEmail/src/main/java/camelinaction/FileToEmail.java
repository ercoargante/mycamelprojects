/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package camelinaction;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class FileToEmail {

	public static void main(String args[]) throws Exception {
		// create CamelContext
		CamelContext context = new DefaultCamelContext();

		// add our route to the CamelContext
		context.addRoutes(new RouteBuilder() {
			public void configure() {

								
				// Route 1: This route sends and email for every file found in the specified 
				// folder, with the file content as email body and a fixed email subject.
				// To make it work: replace the placeholders below with real data
				from("file:folderWithFilesToMail?noop=true")
						.setHeader("subject", simple("Duimen omhoog!!"))
						.to("smtps://<google user name>@smtp.gmail.com?password=<passwd>&to=<email address>&from=camellover@gmail.com");

				// Route 2: This route searches your inbox for emails with a specific 
				// subject. For every match, an entry is added to the log, and for every 
				// match the email content is appended to a file.
				// TODO: if too many files in the inbox match, the route does not give log output and does not create files
				// To make it work: replace the placeholders below with real data
				from(
						"imaps://imap.gmail.com?username=<email address>&password=<passwd>"
								+ "&searchTerm.subjectOrBody='Duimen omhoog!!'"
								+ "&delete=false"
//								+ "&unseen=false"
								+ "&consumer.delay=60000")
						.to("file:studmails"
								+ "?fileExist=Append")
						.to("log:studentmail?level=INFO");

			}
		});

		// start the route and let it do its work
		context.start();
		Thread.sleep(10000);

		// stop the CamelContext
		context.stop();
	}
}
